from fastapi import APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from utils.jwt_manager import create_token
from schemas.user import User
#CREACION DE INSTANCIA
user_router = APIRouter()

@user_router.post('/login', tags=['auth'])  # Cambiado a GET
def login(user: User):
    if user.email == 'admin@gmail.com' and user.password == 'admin':
        token: str = create_token(data={"sub": user.email})
        return JSONResponse(status_code=200, content={"token": token})