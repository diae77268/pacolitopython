from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from fastapi import FastAPI, Request, Response
# from starlette.types import ASGIApp
from starlette.responses import Response
#SOLO SE OCUPA PARE LA IMPORTACIOND EL INIT Y QUE GENERE EL CODIFOD E MANDE AUTOMATICA
from fastapi.responses import JSONResponse

class ErrorHandler(BaseHTTPMiddleware):
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)

    async def dispatch(self, request: Request, call_next) -> Response:
        try:
            return await call_next(request)
        except Exception as e:
            return JSONResponse(status_code=500, content=('error', str(e)))
